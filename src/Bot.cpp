#include <fstream>
#include "sim.hpp"
#include "Message.hpp"

namespace neuro {


Bot::Bot(const BotID id):
  _dis(0, 149),
  _id(id),
  _x(_dis(gen)),
  _y(_dis(gen))
{}

BotID Bot::id() const {
  return _id;
}

decltype(Bot::_x) Bot::x() const {
  return _x;
}
  
decltype(Bot::_y) Bot::y() const {
  return _y;
}

const decltype(Bot::_neightboors)& Bot::neightboors() const {
  return _neightboors;
}

distance Bot::knows (const BotID id) const {
  const auto got = _neightboors.find(id);
  if (got == _neightboors.end()) {
    return 0;
  }

  return got->second;
}
  
distance Bot::dist(decltype(_x) x, decltype(_y) y) const {
  return std::abs(x - _x) + std::abs(y - _y);
}
  
bool Bot::add_neightboor(const Bot &bot) {
  const auto got = _neightboors.insert({
      bot.id(), dist(bot.x(), bot.y())
	});

  return got.second;
}

void Bot::clean_seen_messages() {
  const auto upper_bound = _net->ts() + 500; // TODO parametrize
  auto it = _seen_message_by_ts.upper_bound(upper_bound);
  while (it != _seen_message_by_ts.end()) {
    
  }
}

bool Bot::clear_message(const Message &message) {
  if (message.addrs.size() > 7) {
    return false;
  }

  const auto got = _seen_message.find(message.id);
  return (got == _seen_message.end());
}

void Bot::dispatch(const Message &message) {
  
  if(!clear_message(message)) {
    return;
  }
  
  if (message.dest == _id) {
    std::cout << "received> " << message << std::endl;
    std::cout << *_net << std::endl;
    //exit(0);
    return;					  
  }


  if (const auto dist = knows(message.dest); dist != 0) {
    Message new_message(message);
    new_message.addrs.push_back(message.dest);
    _net->schedule(new_message, dist);
    return;
  }
  
  for (const auto neightboor : neightboors()) {
    const auto preivious_bots = message.addrs;
    const auto got = std::find(preivious_bots.begin(),
			       preivious_bots.end(),
			       neightboor.first);
    if(got != preivious_bots.end()) {
      // message already seen
      continue;
    }
    Message new_message(message);
    new_message.addrs.push_back(neightboor.first);
    
    const auto dist = neightboor.second;
    _net->schedule(new_message, dist);
  }
}
  
Bots::Bots(const uint32_t N) {
  for (uint32_t i = 0 ; i < N ; i++){
    _bots.emplace_back(i);
  }
}

const Bot &Bots::bot(const BotID id) const {
  return _bots[id];
}

Bot *Bots::bot_mutable(const BotID id) {
  return &_bots[id];    
}

bool Bots::is_reachable(const BotID a,
			const BotID b,
			std::vector<BotID> v) const {
  v.push_back(a);
  const auto botA = _bots[a];
  if(a == b) {
    std::cout << "Found path from " << v.front() << " to "
	      << v.back() << " in " << v.size() << " - ";
    for (const auto e : v) {
      std::cout << e << " ";
    }std::cout << std::endl;
    return true;
  }

  for (const auto neightboor : botA.neightboors()) {
    const auto got = std::find(v.begin(), v.end(), neightboor.first);
    if(got == v.end()) {
      if(is_reachable(neightboor.first, b, v)) {
	return true;
      }
    }
  }
  return false;
}

bool Bots::is_connected() {
  dlib::directed_graph<int>::kernel_1a_c graph;
  graph.set_number_of_nodes(_bots.size());

  for (const auto bot : _bots) {
    for (const auto neightboor: bot.neightboors()) {
      graph.add_edge(bot.id(), neightboor.first);
    }
  }

  return dlib::graph_is_connected(graph);
}

void Bots::graphViz(const std::string &path) const {
  std::ofstream fp(path);

  if(!fp.is_open()) {
    std::cout << "Could not open " << path << std::endl;
    return;
  }

  fp << "diGraph G {";
  for (const auto bot : _bots) {
    for (const auto neightboor : bot.neightboors()) {
      fp << bot.id() << " -> " << neightboor.first << std::endl;
    }
  }
  fp << "}";
}

Net* Bot::_net = nullptr;

}  // neuro
