#ifndef NEURO_SRC_BOT_HPP
#define NEURO_SRC_BOT_HPP

#include <unordered_set>

#include <dlib/graph_utils.h>
#include <dlib/graph.h>
#include <dlib/directed_graph.h>

#include <unordered_map>
#include "def.hpp"
#include "Message.hpp"

namespace neuro {

class Net;

class Bot {
public:
  static Net *_net;
  
private:
  std::uniform_int_distribution<> _dis;
  const BotID _id;
  const int64_t _x;
  const int64_t _y;
  std::unordered_map<BotID, distance> _neightboors;
  std::unordered_set<Message::ID> _seen_message;
  std::map<std::time_t, Message::ID> _seen_message_by_ts;

  bool clear_message(const Message &message);
  void clean_seen_messages();
  
public:
  Bot(const BotID id);
  BotID id() const;
  decltype(_x) x() const;
  decltype(_y) y() const;

  const decltype(_neightboors)& neightboors() const;

  distance knows (const BotID id) const;  
  distance dist(decltype(_x) x, decltype(_y) y) const;
  bool add_neightboor(const Bot &bot);
  void dispatch(const Message &message);
  void connect(const BotID id);

  friend class TestBot;
};


class Bots {
private:
  std::vector<Bot> _bots;

public:
  Bots(const uint32_t N);
  const Bot &bot(const BotID id) const;
  Bot *bot_mutable(const BotID id);  
  void graphViz(const std::string &path) const;

  decltype(_bots.size()) size() const {
    return _bots.size();
  }
  
  decltype(_bots.begin()) begin() {
    return _bots.begin();
  }
  decltype(_bots.end()) end() {
    return _bots.end();
  }

  bool is_reachable(const BotID a,
		    const BotID b,
		    std::vector<BotID> v = {}) const;
  
  bool is_connected();
};

}  // neuro

#endif /* NEURO_SRC_BOT_HPP */
