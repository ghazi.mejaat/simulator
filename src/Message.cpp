#include "Message.hpp"


namespace neuro {

std::ostream& operator<< (std::ostream &os, const Message &message) {
  os << "Message> " << message.id << std::endl;
  os << "Intermediate ";
  for (const auto &bot_id : message.addrs) {
    os << " -> " <<  bot_id ;
  } os << message.dest << std::endl;
  os << "==============================" << std::endl;
  return os;
}


}  // neuro
