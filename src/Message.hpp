#ifndef NEURO_SRC_MESSAGE_HPP
#define NEURO_SRC_MESSAGE_HPP

#include <memory>
#include <iostream>

#include "def.hpp"

namespace neuro {

struct Message {
  using ID = uint32_t;
  
  const ID id;
  BotID dest;
  Message(const ID id, const BotID botid) :
    id(id),
    dest(botid){}
  
  std::vector<BotID> addrs;
};

std::ostream& operator<< (std::ostream &os, const Message &message);

}  // neuro

#endif /* NEURO_SRC_MESSAGE_HPP */
