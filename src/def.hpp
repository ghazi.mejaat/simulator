#ifndef NEURO_SRC_DEF_HPP
#define NEURO_SRC_DEF_HPP

#include <random>
#include <ctime>
namespace neuro {


using std::int8_t;
using std::int16_t;
using std::int32_t;
using std::int64_t;

using std::uint8_t;
using std::uint16_t;
using std::uint32_t;
using std::uint64_t;

using latency = uint16_t;
using distance = uint64_t;
static std::random_device rd;
static std::mt19937 gen(std::time(nullptr));

using BotID = uint32_t;

}  // neuro

#endif /* NEURO_SRC_DEF_HPP */
