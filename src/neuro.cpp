#include "sim.hpp"

namespace neuro {

int main(int argc, char *argv[]) {
  Simulator simulator(300000, 7, 300);
  
  return 0;
}

}  // neuro

int main(int argc, char *argv[]) {
  
  return neuro::main(argc, argv);
}
