#ifndef NEURO_SRC_SIM_HPP
#define NEURO_SRC_SIM_HPP

#include <vector>
#include <queue>
#include <cstdint>
#include <random>
#include <stdexcept>
#include <iostream>
#include <unordered_map>
#include <cmath>
#include <list>
#include <ctime>
#include <memory>

#include "def.hpp"
#include "Bot.hpp"
#include "Net.hpp"

namespace neuro {

class Simulator {
private:
  Bots _bots;
  Net _net;
  uint64_t _msgs_received;
  uint64_t _msgs_sent;
  
public:
  Simulator(const uint32_t bots_count,
	    const uint8_t bot_connection_count,
	    const latency max_time);  
  bool process();
  void connect_random(const uint8_t bot_connection_count);
  void send(const BotID src, const BotID dst);  
  void run ();
};


}  // neuro

#endif /* NEURO_SRC_SIM_HPP */
